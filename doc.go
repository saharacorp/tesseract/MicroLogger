/*
The MicroLogger package is a Go library intended to provide a
simple and uniform way of logging data to the console for
debugging or status updates. The style is based off of Linux
boot sequences. The library also follows the Syslog Standard

There are multiple different log severity levels.
DEBUG, INFO, NOTICE, WARN, ERR, CRIT, ALERT, EMERG, and EXTENSION.
These are in a type called LoggerLevel.
	Log(DEBUG, "This is a log entry with the severity level 'DEBUG'")
	Log(INFO, "This is a log entry with the severity level 'INFO'")
	Log(EXTENSION, "This is a log entry with the severity level 'EXTENSION'")
	Log(NOTICE, "This is a log entry with the severity level 'NOTICE'")
	Log(WARN, "This is a log entry with the severity level 'WARN'")
	Log(ERR, "This is a log entry with the severity level 'ERR'")
	Log(CRIT, "This is a log entry with the severity level 'CRIT'")
	Log(ALERT, "This is a log entry with the severity level 'ALERT'")
	Log(EMERG, "This is a log entry with the severity level 'EMERG'")

The colors for the CRIT, ALERT, and EMERG logger level can be found in
CriticalColor, AlertColor, and EmergencyColor respectively.

More information, along with detailed usage examples can be found at
https://gitlab.com/infinitisoftware/tesseract/MicroLogger/-/wikis/home
*/
package log